package com.backupSys.mapper;

import com.backupSys.pojo.Backup;
import java.util.List;

public interface BackupMapper {

    public void add(Backup backup);
    public void delete(int userid);
    public void updateBackup(Backup backup);
    public List<Backup>  getBackups(int userid);

}
