package com.backupSys.mapper;

import com.backupSys.pojo.User;
import java.util.List;

public interface UserMapper {

   public  void add(User user);
   public void delete(int id);
   public User getUser(int id);
   public void updateUser(User user);
   public List<User> getUsers();

}
