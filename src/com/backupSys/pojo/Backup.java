package com.backupSys.pojo;

import java.util.Date;

public class Backup {
    private int id;
    private int userid;
    private String conent;
    private Date calldate;
    private int done;
    private Date lstime;

    public int getId() {
        return id;
    }

    public int getUserid() {
        return userid;
    }

    public String getConent() {
        return conent;
    }

    public Date getCalldate() {
        return calldate;
    }

    public int getDone() {
        return done;
    }

    public Date getLstime() {
        return lstime;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public void setConent(String conent) {
        this.conent = conent;
    }

    public void setCalldate(Date calldate) {
        this.calldate = calldate;
    }

    public void setDone(int done) {
        this.done = done;
    }

    public void setLstime(Date lstime) {
        this.lstime = lstime;
    }
}
