package com.backupSys.service.Impl;

import com.backupSys.mapper.BackupMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.backupSys.pojo.Backup;
import com.backupSys.service.BackupService;

import java.util.List;

@Service
public class BackupServiceImpl implements BackupService {
    @Autowired
    BackupMapper backupMapper;

    @Override
    public void add(Backup backup) {
        backupMapper.add(backup);
    }

    @Override
    public void delete(int userid) {
        backupMapper.delete(userid);
    }

    @Override
    public void updateBackup(Backup backup) {
        backupMapper.updateBackup(backup);
    }

    @Override
    public List<Backup> getBackups(int userid) {
        return backupMapper.getBackups(userid);
    }
}
