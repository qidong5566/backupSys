package com.backupSys.service;

import com.backupSys.pojo.Backup;

import java.util.List;

public interface BackupService {
    public void add(Backup backup);
    public void delete(int userid);
    public void updateBackup(Backup backup);
    public List<Backup> getBackups(int userid);
}
