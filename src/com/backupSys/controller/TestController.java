package com.backupSys.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import com.backupSys.pojo.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class TestController {

    public static  void main(String[] args) throws IOException {
        String resource = "mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = sqlSessionFactory.openSession();

        List<User> cs = session.selectList("getUsers");
        for (User c : cs) {
            System.out.println(c.getUsername());
            System.out.print("adadad");
        }
    }
}
