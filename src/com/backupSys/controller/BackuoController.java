package com.backupSys.controller;

import com.backupSys.pojo.Backup;
import com.backupSys.service.BackupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping("")
public class BackuoController {

    @Autowired
    BackupService backupService;


    public @ResponseBody List<Backup> GetBackupInfo()
    {
        List<Backup> tempList=backupService.getBackups(1);
        if(tempList==null)
        {
            return null;
        }
        return tempList;
    }
}
