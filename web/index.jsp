<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<script src="http://how2j.cn/study/js/jquery/2.0.0/jquery.min.js"></script>
<link href="http://how2j.cn/study/css/bootstrap/3.3.6/bootstrap.min.css" rel="stylesheet">
<script src="http://how2j.cn/study/js/bootstrap/3.3.6/bootstrap.min.js"></script>

<html>
<head>
    <meta charset="utf-8">
    <title>login</title>
</head>
<body>
<div class="container">
    <h1 class="page-header">登录界面</h1>
    <form>
        <div class="row">
            <div class="form-group col-sm-7">
                <label for="username">账号</label>
                <input type="text" class="form-control" id="username" placeholder="请输入账号"/>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-sm-7">
                <label for="passowrd">密码</label>
                <input type="password" class="form-control" id="passowrd" placeholder="请输入密码"/>
            </div>
        </div>
        <div class="row">
            <input type="submit" class="btn btn-info btn-lg col-sm-1 col-sm-offset-1" value="登录"/>\
            <a href="index.jsp" class="btn btn-lg btn-danger col-sm-1 col-sm-offset-3">注册</a>
        </div>
        <div class="row">

        </div>
    </form>
</div>
</body>
</html>